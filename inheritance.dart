import 'Armor_Titan.dart';
import 'Attack_Titan.dart';
import 'Beast_Titan.dart';
import 'Human.dart';

void main(List<String> args) {
  Armor_Titan a = Armor_Titan();
  Attack_Titan b = Attack_Titan();
  Beast_Titan c = Beast_Titan();
  Human d = Human();
  a.levelPoint = 2;
  b.levelPoint = 3;
  c.levelPoint = 4;
  d.levelPoint = 5;

  print("Level point Armor Titan : ${a.levelPoint}");
  print("Level point Attack Titan : ${b.levelPoint}");
  print("Level point Beast Titan : ${c.levelPoint}");
  print("Level point Human : ${d.levelPoint}");

  print("Sifat dari Armor Titan : " + a.terjang());
  print("Sifat dari Attack Titan : " + b.punch());
  print("Sifat dari Beast Titan : " + c.lempar());
  print("Sifat dari Human : " + d.killallTitan());
}
