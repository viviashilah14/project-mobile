class ClassLingkaran {
  late double r;
  late double phi;
  void setr(double value) {
    if (value < 0) {
      value *= -1;
    }
    r = value;
  }

  double getr() {
    return r;
  }

  void setphi(double value) {
    if (value < 0) {
      value *= -1;
    }
    phi = value;
  }

  double getphi() {
    return phi;
  }

  double hitungLuas() {
    return this.phi * r * r;
  }
}
