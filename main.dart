import 'bangun_datar.dart';
import 'Lingkaran.dart';
import 'Persegi.dart';
import 'Segitiga.dart';

void main(List<String> args) {
  bangun_datar bangunDatar = new bangun_datar();
  Lingkaran lingkaran = new Lingkaran(10, 3.14);
  Persegi persegi = new Persegi(10);
  Segitiga segitiga = new Segitiga(0.5, 10, 20, 10);

  bangunDatar.luas();
  print("Luas lingkaran : ${lingkaran.luas()}");
  print("Luas persegi : ${persegi.luas()}");
  print("Luas segitiga : ${segitiga.luas()}");

  bangunDatar.keliling();
  print("Keliling lingkaran : ${lingkaran.keliling()}");
  print("Keliling persegi : ${persegi.keliling()}");
  print("Keliling segitiga : ${segitiga.keliling()}");
}
